
import { SkNavbarImpl }  from '../../sk-navbar/src/impl/sk-navbar-impl.js';

export class AntdSkNavbar extends SkNavbarImpl {

    get prefix() {
        return 'antd';
    }

    get suffix() {
        return 'navbar';
    }

    get rootEl() {
        if (! this._rootEl) {
            this._rootEl = this.comp.el.querySelector('.ant-drawer');
        }
        return this._rootEl;
    }

    get contentWrapperEl() {
        if (! this._contentWrapperEl) {
            this._contentWrapperEl = this.comp.el.querySelector('.ant-drawer-content-wrapper');
        }
        return this._contentWrapperEl;
    }

    get bodyEl() {
        if (! this._bodyEl) {
            this._bodyEl = this.comp.el.querySelector('.ant-drawer-body');
        }
        return this._bodyEl;
    }

    open() {
        this.rootEl.classList.add('ant-drawer-open');
        this.contentWrapperEl.style.transform = null;
        this.panelEl.classList.add('sk-navbar-panel-open');
    }

    close() {
        this.rootEl.classList.remove('ant-drawer-open');
        if (this.comp.getAttribute('align') === 'right') {
            this.contentWrapperEl.style.transform = 'translateX(100%)';
        } else {
            this.contentWrapperEl.style.transform = 'translateX(-100%)';
        }
        this.panelEl.classList.remove('sk-navbar-panel-open');
    }

    bindAutoClose() {
        if (this.autoCloseHandler) {
            document.removeEventListener('click', this.autoCloseHandler);
        }
        this.autoCloseHandler = document.addEventListener('click', function(event) {
            if (event.target.tagName !== 'SK-BUTTON' && event.target.tagName !== 'BUTTON') {
                if (this.comp.hasAttribute('open')) {
                    this.comp.removeAttribute('open');
                    this.close();
                }
            }
        }.bind(this));
    }

    afterRendered() {
        super.afterRendered();
        this.bodyEl.innerHTML = this.contentsState || this.comp.contentsState;
        this.bindAutoClose();
        this.setAlignStyling();
        if (this.comp.hasAttribute('panel')) {
            this.rootEl.classList.add('has-panel');
        }
        this.renderPanelTpl();
        if (this.comp.getAttribute('sticky') !== 'false') {
            this.bindSticky(this.panelEl, 'panel');
            this.bindSticky(this.bodyEl, 'body');
        }
    }

    setAlignStyling() {
        let align = this.comp.getAttribute('align') || 'left';
        if (align === 'right') {
            this.rootEl.classList.remove('ant-drawer-left');
            this.rootEl.classList.add('ant-drawer-right');
            this.contentWrapperEl.style.transform = 'translateX(100%)';
            this.contentWrapperEl.style.width = "256px;";
        } else if (align === 'top') {
            this.rootEl.classList.remove('ant-drawer-left');
            this.rootEl.classList.add('ant-drawer-top');
            this.contentWrapperEl.style.height = "256px";
        } else if (align === 'bottom') {
            this.rootEl.classList.add('ant-drawer-bottom');
            this.contentWrapperEl.style.height = "256px";
        } else {
            this.contentWrapperEl.style.width = "256px;";
        }

        if (this.comp.hasAttribute('panel')) {
            this.panelEl.classList.add('sk-navbar-panel-' + align);
        }
        this.setContentLength(align);
    }
}
